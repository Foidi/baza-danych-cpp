// struktury - struct
#include <iostream>
#include <bits/stdc++.h>
#include <conio.h>
using namespace std;

  struct karta
  {
      string danie;
      string skladnik1;
      string skladnik2;
      string skladnik3;
      int cena;
  };

  void eksport(vector <karta> pozycje)
{
      ofstream plikXml;
      plikXml.open("dane.xml");
      plikXml<<"<?xml version="<<char(34)<<"1.0"<<char(34)<<" encoding="<<char(34)<<"UTF-8"<<char(34)<<"?>"<<endl;
      plikXml<<"<karta_restauracyjna>"<<endl;
      for(int i=0;i<pozycje.size();i++)
  {
      plikXml<<"<Danie>"<<endl;
      plikXml<<"\t"<<"<nazwa>"<<pozycje[i].danie<<"</nazwa>"<<endl;
      plikXml<<"\t"<<"<skladnik1>"<<pozycje[i].skladnik1<<"</skladnik1>"<<endl;
      plikXml<<"\t"<<"<skladnik2>"<<pozycje[i].skladnik2<<"</skladnik2>"<<endl;
      plikXml<<"\t"<<"<skladnik3>"<<pozycje[i].skladnik3<<"</skladnik3>"<<endl;
      plikXml<<"\t"<<"<cena>"<<pozycje[i].cena<<"</cena>"<<endl;
      plikXml<<"</Danie>"<<endl;
  }
  plikXml<<"</karta_restauracyjna>";
}

  void wczytaj(vector <karta> &pozycje)
  {
      ifstream plikZapis;
      plikZapis.open("dane.txt", ifstream::app);
      karta dod;
    while(!plikZapis.eof())
  {
      plikZapis>>dod.danie;
      plikZapis>>dod.skladnik1;
      plikZapis>>dod.skladnik2;
      plikZapis>>dod.skladnik3;
      plikZapis>>dod.cena;
      pozycje.push_back(dod);

  }
  plikZapis.close();
  }


  void edytuj (vector <karta> &pozycje, int i)
  {
      karta podmien;
      cout<<"podmien nazwe"<<endl;
      cin>>podmien.danie;
      pozycje[i].danie=podmien.danie;
      cout<<"podmien skladnik 1"<<endl;
      cin>>podmien.skladnik1;
      pozycje[i].skladnik1=podmien.skladnik1;
      cout<<"podmien skladnik 2"<<endl;
      cin>>podmien.skladnik2;
      pozycje[i].skladnik2=podmien.skladnik2;
      cout<<"podmien skladnik 3"<<endl;
      cin>>podmien.skladnik3;
      pozycje[i].skladnik3=podmien.skladnik3;
      cout<<"podmien cene"<<endl;
      cin>>podmien.cena;
      pozycje[i].cena=podmien.cena;
  }

  void usun(vector <karta> &pozycje, int i)
  {
      pozycje.erase(pozycje.begin()+i);
  }


int znajdz(vector <karta> pozycje, string x)
{
    for(int i=0;i<pozycje.size();i++)
    {
        if(x==pozycje[i].danie)
        {
            return i;
        }
    }
    return -1;
}

  void sortuj(vector <karta> &pozycje, char x)
  {
      for(int z=0;z<pozycje.size();z++)
      {
      if(x=='a')
      {
          int j=1;
          for(int i=0;i<pozycje.size()-1;i++,j++)
          {
              string temp1,temp2;
              temp1=pozycje[i].danie;
              temp2=pozycje[j].danie;
              if(temp1.compare(temp2)>0)
              {
                  swap(pozycje[i],pozycje[j]);


              }

          }
      }


      if(x=='b')
      {
          int j=1;
          for(int i=0;i<pozycje.size()-1;i++,j++)
          {
              string temp1,temp2;
              temp1=pozycje[i].skladnik1;
              temp2=pozycje[j].skladnik1;
              if(temp1.compare(temp2)>0)
              {
                  swap(pozycje[i],pozycje[j]);

              }

          }
      }


      if(x=='c')
      {
          int j=1;
          for(int i=0;i<pozycje.size()-1;i++,j++)
          {
              string temp1,temp2;
              temp1=pozycje[i].skladnik2;
              temp2=pozycje[j].skladnik2;
              if(temp1.compare(temp2)>0)
              {
                  swap(pozycje[i],pozycje[j]);

              }

          }
      }



      if(x=='d')
      {
          int j=1;
          for(int i=0;i<pozycje.size()-1;i++,j++)
          {
              string temp1,temp2;
              temp1=pozycje[i].skladnik3;
              temp2=pozycje[j].skladnik3;
              if(temp1.compare(temp2)>0)
              {
                  swap(pozycje[i],pozycje[j]);

              }

          }
      }

      if(x=='e')
      {
          int j=1;
          for(int i=0;i<pozycje.size()-1;i++,j++)
          {
              string temp1,temp2;
              temp1=pozycje[i].cena;
              temp2=pozycje[j].cena;
              if(temp1.compare(temp2)>0)
              {
                  swap(pozycje[i],pozycje[j]);

              }

          }
      }
  }
  }

  void dodajPozycje(vector <karta> &pozycje)
  {
    ofstream plikZapis;
    plikZapis.open("dane.txt", fstream::app);
    karta rest;
    cout<<"Podaj danie:   ";
    cin>>rest.danie;
    plikZapis<<rest.danie<<endl;
    cout<<"Podaj pierwszy skladnik: ";
    cin>>rest.skladnik1;
    plikZapis<<rest.skladnik1<<endl;
    cout<<"Podaj  drugi   skladnik: ";
    cin>>rest.skladnik2;
    plikZapis<<rest.skladnik2<<endl;
    cout<<"Podaj  trzeci  skladnik: ";
    cin>>rest.skladnik3;
    plikZapis<<rest.skladnik3<<endl;
    cout<<"Podaj  cene:   ";
    cin>>rest.cena;
    plikZapis<<rest.cena;
    plikZapis.close();
    pozycje.push_back(rest);
  }
int main()
{
  vector <karta> pozycje;
  pozycje.erase(pozycje.begin(),pozycje.end());
  wczytaj(pozycje);
  pozycje.erase(pozycje.begin()+pozycje.size());
  string szukaj;
  int v=pozycje.size();
  while(true)
  {
      ofstream plikZapis;
      plikZapis.open("dane.txt");
      v=pozycje.size();
      for(int i=0;i<v;i++)
      {
          plikZapis<<pozycje[i].danie<<endl;
          plikZapis<<pozycje[i].skladnik1<<endl;
          plikZapis<<pozycje[i].skladnik2<<endl;
          plikZapis<<pozycje[i].skladnik3<<endl;
          plikZapis<<pozycje[i].cena<<endl;
      }
      plikZapis.close();
      system("cls");
 cout<<"|[==========================]|"<<endl;
 cout<<"|| Ilosc pozycji w bazie: "<<pozycje.size()<<" ||"<<endl;
 cout<<"||__________________________||"<<endl;
 cout<<"||                          ||"<<endl;
 cout<<"|| 1 - WYPISZ DANIA         ||"<<endl;
 cout<<"|| 2 - DODAJ DANIE          ||"<<endl;
 cout<<"|| 8 - USUN WSZYSTKIE WPISY ||"<<endl;
 cout<<"|| 0 - ZAMKNIJ PROGRAM      ||"<<endl;
 cout<<"||__________________________||"<<endl;
 cout<<"|| Wypisz dania sortujac po:||"<<endl<<"||'a' - NAZWA               ||"<<endl<<"||'b' - SKLADNIK 1.         ||"<<endl<<"||'c' - SKLADNIK 2.         ||"<<endl<<"||'d' - SKLADNIK 3.         ||"<<endl<<"||'e' - CENA                ||"<<endl;
 cout<<"|[==========================][==============]|"<<endl;
 cout<<"|| Wcisnij 's' aby wyszukac danie po nazwie ||"<<endl;
 cout<<"|| Wcisnij 'x' aby wyeksportowac do .xml    ||"<<endl;
 cout<<"|[==========================================]|"<<endl;
 cout<<"  Wybierz opcje: "<<endl;
 char znak = getch();
 switch (znak)
 {
     case '1':
        cout<<endl<<"aktualna lista: "<<endl;
        {for(int i=0;i<pozycje.size();i++)
  {
      cout<<pozycje[i].danie<<" - ";
      cout<<pozycje[i].skladnik1<<", ";
      cout<<pozycje[i].skladnik2<<", ";
      cout<<pozycje[i].skladnik3<<" | ";
      cout<<pozycje[i].cena<<" zl"<<endl;
  }
  cout<<getch();
  break;
     }
     case '2': dodajPozycje(pozycje);
     break;
     case '0': return 0;
     case 'a':cout<<"Aktualne dania :"<<endl;
     sortuj(pozycje,'a');
     {for(int i=0;i<pozycje.size();i++)
  {
      cout<<pozycje[i].danie<<" - ";
      cout<<pozycje[i].skladnik1<<", ";
      cout<<pozycje[i].skladnik2<<", ";
      cout<<pozycje[i].skladnik3<<" | ";
      cout<<pozycje[i].cena<<" zl"<<endl;
  }
     cout<<getch();
     break;
     case 'b':cout<<"Aktualne dania :"<<endl;
     sortuj(pozycje,'b');
     {for(int i=0;i<pozycje.size();i++)
  {
      cout<<pozycje[i].danie<<" - ";
      cout<<pozycje[i].skladnik1<<", ";
      cout<<pozycje[i].skladnik2<<", ";
      cout<<pozycje[i].skladnik3<<" | ";
      cout<<pozycje[i].cena<<" zl"<<endl;
  }
     cout<<getch();
     break;
     case 'c':cout<<"Aktualne dania :"<<endl;
     sortuj(pozycje,'c');
     {for(int i=0;i<pozycje.size();i++)
  {
      cout<<pozycje[i].danie<<" - ";
      cout<<pozycje[i].skladnik1<<", ";
      cout<<pozycje[i].skladnik2<<", ";
      cout<<pozycje[i].skladnik3<<" | ";
      cout<<pozycje[i].cena<<" zl"<<endl;
  }
     cout<<getch();
     break;
     case 'd':cout<<"Aktualne dania :"<<endl;
     sortuj(pozycje,'d');
     {for(int i=0;i<pozycje.size();i++)
  {
      cout<<pozycje[i].danie<<" - ";
      cout<<pozycje[i].skladnik1<<", ";
      cout<<pozycje[i].skladnik2<<", ";
      cout<<pozycje[i].skladnik3<<" | ";
      cout<<pozycje[i].cena<<" zl"<<endl;
  }
     cout<<getch();
     break;
     case 'e':cout<<"Aktualne dania :"<<endl;
     sortuj(pozycje,'e');
     {for(int i=0;i<pozycje.size();i++)
  {
      cout<<pozycje[i].danie<<" - ";
      cout<<pozycje[i].skladnik1<<", ";
      cout<<pozycje[i].skladnik2<<", ";
      cout<<pozycje[i].skladnik3<<" | ";
      cout<<pozycje[i].cena<<" zl"<<endl;
  }
     cout<<getch();
     break;
     cout<<getch();
     case 's':
         cout<<"wpisz szukane danie: "<<endl;
         cin>>szukaj;
         if(znajdz(pozycje,szukaj)<0)
         {
             cout<<"nie ma";
             cout<<getch();
             break;
         }
         cout<<"jest"<<endl;
         cout<<"Edytowac lub usunac czy nic nie robic? [e/u/n]"<<endl;
         char edt;
         edt=getch();
         if(edt=='e')
         {
             edytuj(pozycje,znajdz(pozycje,szukaj));
             break;
         }
         else if(edt=='u')
         {
             usun(pozycje,znajdz(pozycje,szukaj));
             break;
         }
         cout<<getch();
         break;
     case 'x': eksport(pozycje);
     break;
     case '8': cout<<"na pewno? (t/n)"<<endl;
     if(getch()=='t')
      {
          ofstream plik;
          plik.open("dane.txt", fstream::trunc);
          plik.close();
          pozycje.erase(pozycje.begin(),pozycje.end());
          cout<<"Pomyslnie usunieto";
          getch();
      }

      else break;
 }
  }
}
}
}
}
}
}
